import { Component } from '@angular/core';
import { Animal } from './animal';
import { Sector } from './sector';

@Component({
  selector: 'animal-list',
  templateUrl: 'animal-list.html'
})
export class AnimalListComponent {

  public animals: Animal[] = [];
  public sectors: Sector[] = [];

  public newAnimal: Animal = new Animal('', '', '', null);

  constructor() {
    this.createSectors();
    this.createAnimals();
  }

  private createSectors()
  {
    this.sectors.push(new Sector('Sever', 10000));
    this.sectors.push(new Sector('Jug', 9000));
    this.sectors.push(new Sector('Istok', 11000));
    this.sectors.push(new Sector('Centar', 11000));
    this.sectors.push(new Sector('Zapad', 10000));
  }

  private createAnimals()
  {
    this.animals.push(new Animal('lav', 'Cicko', '01.01.2017.', this.sectors[3]));
    this.animals.push(new Animal('zirafa', 'Loli', '01.02.2017.', this.sectors[3]));
    this.animals.push(new Animal('slon', 'Bole', '01.03.2017.', this.sectors[4]));
    this.animals.push(new Animal('majmum', '', '01.01.2016.', this.sectors[0]));
    this.animals.push(new Animal('vuk', 'Vucko', '01.01.2014.', this.sectors[1]));
    this.animals.push(new Animal('lisica', 'Lidija', '01.01.2013.', this.sectors[4]));
    this.animals.push(new Animal('medved', 'Brundo', '01.01.2012.', this.sectors[1]));
    this.animals.push(new Animal('beli medved', 'Misko', '12.01.2017.', this.sectors[3]));
    this.animals.push(new Animal('orao', 'Srba', '01.08.2016.', this.sectors[0]));
    this.animals.push(new Animal('jelen', 'Pivo', '01.03.2013.', this.sectors[1]));
  }

  public addAnimal(newAnimal: Animal)
  {
    this.animals.push(new Animal(newAnimal.Species, newAnimal.Name, newAnimal.DateOfBirth, newAnimal.Sector));

    newAnimal.Species = '';
    newAnimal.Name = '';
    newAnimal.DateOfBirth = '';
    newAnimal.Sector = null;
  }

  public removeAnimal(animal: Animal)
  {
    const index = this.animals.indexOf(animal);

    if (index > -1) {
      return this.animals.splice(index, 1);
    }
  }

  public moveToTop(animal: Animal)
  {
    let removedArr = this.removeAnimal(animal);

    if (removedArr) {
      this.animals.unshift(removedArr[0]);
    }
  }

  public checkSectorAnimals(sector: Sector)
  {
    let res = [];

    this.animals.forEach((animal) => {
        if (animal.Sector && animal.Sector.Name == sector.Name) {
          res.push(animal.Species + ' ' + animal.Name);
        }
    });

    if (res.length) {
      alert('Lista zivotinja u sektoru ' + sector.Name + '\n\n' + res.join('\n'));
    } else {
      alert('Nema zivotinja u sektoru ' + sector.Name);
    }
  }

}
