export class Sector {
  Name: string;
  Surface: number;

  constructor(name: string, surface: number) {
  	this.Name = name;
  	this.Surface = surface;
  }
}
