import { Sector } from './sector';

export class Animal {
  Species: string;
  Name: string;
  DateOfBirth: string;
  Sector: Sector;

  constructor(species: string, name: string, dateOfBirth: string, sector: Sector) {
  	this.Species = species;
  	this.Name = name;
  	this.DateOfBirth = dateOfBirth;
  	this.Sector = sector;
  }
}
